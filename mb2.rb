require "highline/import"
require 'state_machine'
class Deck
  attr_accessor :cards
  def initialize()
    descriptions = [
    ["ACCIDENT",3,"ATTAQUE","Accident!"],
    ["PANNE",3,"ATTAQUE","En panne !"],
    ["CREVAISON",3,"ATTAQUE",""],
    ["LIMIT_VIT",4,"ATTAQUE",""],
    ["FEU_ROUGE",5,"ATTAQUE",""],
    ["REPARATION",6,"DEFENSE",""],
    ["ESSENCE",6,"DEFENSE",""],
    ["ROUE_SEC",6,"DEFENSE",""],
    ["LIMIT_FIN",6,"DEFENSE",""],
    ["FEU_VERT",14,"DEFENSE",""],
    ["AS_DU_VOLANT",1,"BOTTE",""],
    ["INCREVABLE",1,"BOTTE",""],
    ["CITERNE",1,"BOTTE",""],
    ["PRIORITAIRE",1,"BOTTE",""],
    ["25",10,"DISTANCE",""],
    ["50",10,"DISTANCE",""],
    ["75",10,"DISTANCE",""],
    ["100",12,"DISTANCE",""],
    ["200",4,"DISTANCE",""]]
    puts "New deck created..."
    @cards=[]

    #Generating deck :
    descriptions.each  do |description|
      for i in 1..description[1] do
        une_carte=Carte.new
        une_carte.libelle=description[0]
        une_carte.type=description[2]
        add_card(une_carte)
      end
    end
    #calling self method "melange"
    melange
    end

  def add_card(card)
    #append 'card' object to 'cards' array.
    @cards << card
  end

  def melange()
    puts "Cards shuffled..."
    @cards.shuffle!
  end

  def afficher_jeu()

    for i in 1..(@cards.count) do
      puts "#{@cards[i-1].libelle} -> #{@cards[i-1].state}"
    end
  end




  def choose_card()
    # point on the last card (0)
    i=0
    while i<@cards.count && @cards[i].state != "FREE" do
      i=i+1
    end
    if (i==(@cards.count)) then
      puts "plus de carte !!!"
      # ICI, on va reseter les DROPPED -> FREE
      return false
    else
      @cards[i].state = "TAKEN"
      return @cards[i]
    end

  end

  def defausser(carte)
    carte.state = "DROPPED"
  end

  def jouer(carte)
    carte.state = "PLAYED"
  end

end
class Player
  attr_accessor :name, :hand, :distance, :state


  def initialize(*args)
    @hand = []
    @name = args[0]
    @distance=0
    @state= "WAITING"
    for i in 1..6 do
      @hand << $mon_deck.choose_card()
    end
  end

  def play_card(card)

    # carte Distance.
    if (card.libelle=="DISTANCE") then
      @distance=@distance+card.libelle.to_i
    end

  end

  def send_card(card,player)


  end



  def pioche()
    @hand << $mon_deck.choose_card()
  end

  def can_play()
    #Chack which card in hand are suitable
    # TAgs playables cards in hand
    #if any : return false

    else
    # none is playable : chose defausse
    return true
  end


  def my_turn()
    pioche()
    if can_play()==false then
      ask_defausse()
    else
      ask_play()
    end
  end

  def afficher_main()
    i=0
    for card in @hand do
      if card.playable == "OUI"
      puts "#{i} - #{card.libelle}"
      i=i+1
      end
    end

  end

  def ask_play()
    puts "JOUER :"
    afficher_main()
    input = ask "-> Carte à jouer : "
    carte_jouer= @hand[(input.to_i-1)]
    $mon_deck.jouer(carte_jouer)
    play_card(carte_jouer)


    @hand.delete_at(input.to_i-1)

  end

  def ask_defausse()
    puts "DEFAUSSER :"
    afficher_main()
    input = ask "-> Carte à defausser : "
    carte_defausser = @hand[(input.to_i-1)]

    $mon_deck.defausser(carte_defausser)
    @hand.delete_at(input.to_i-1)
  end
end
class Carte
  attr_accessor :libelle, :type, :state
  #state = FREE / PLAYED / DROPPED / TAKEN
  def initialize()
    @state='FREE'
  end
end
$mon_deck = Deck.new
#  Initialisation
playerA = Player.new("Joueur A")
playerB = Player.new("Joueur B")
playerC = Player.new("Joueur C")
playerD = Player.new("Joueur D")
# On commence à jouer
while true do
  puts "JOUEUR A (#{playerA.distance})"
  playerA.my_turn()
  $mon_deck.afficher_jeu()
  puts "JOUEUR B (#{playerB.distance})"
  playerB.my_turn()
  $mon_deck.afficher_jeu()
end
