require "highline/import"


class Deck
  attr_accessor :cards
  def initialize()

  descriptions = [
  ["ACCIDENT",3,"ATTAQUE"],
  ["PANNE",3,"ATTAQUE"],
  ["CREVAISON",3,"ATTAQUE"],
  ["LIMIT_VIT",4,"ATTAQUE"],
  ["FEU_ROUGE",5,"ATTAQUE"],
  ["REPARATION",6,"DEFENSE"],
  ["ESSENCE",6,"DEFENSE"],
  ["ROUE_SEC",6,"DEFENSE"],
  ["LIMIT_FIN",6,"DEFENSE"],
  ["FEU_VERT",14,"DEFENSE"],
  ["AS_DU_VOLANT",1,"BOTTE"],
  ["INCREVABLE",1,"BOTTE"],
  ["CITERNE",1,"BOTTE"],
  ["PRIORITAIRE",1,"BOTTE"],
  ["25KM",10,"DISTANCE"],
  ["50KM",10,"DISTANCE"],
  ["75KM",10,"DISTANCE"],
  ["100KM",12,"DISTANCE"],
  ["200KM",4,"DISTANCE"]]

    puts "New Deck created !"
    @cards=[]

 #Generating deck
descriptions.each do |description|
  for i in 1..description[1] do
      une_carte=Carte.new
      une_carte.libelle=description[0]
      une_carte.type=description[2]
      $mon_deck.add_card(une_carte)
  end
end

#calling self method "melange"
    melange
    end

  def add_card(card)
    @cards << card
  end

  def melange()
    puts "Cards shuffled..."
    @cards.shuffle!
  end

  def afficher_jeu()
    for i in 1..(@cards.count) do
      puts "#{@cards[i-1].libelle} -> #{@cards[i-1].state} : #{@cards[i-1].playable}"
    end
  end

  def pick_card()
    # point towards the last card (0)
    i=0
    while i<@cards.count && @cards[i].state != "FREE" do
      i=i+1
    end
    if (i==(@cards.count)) then
      puts "plus de carte !!!"
      # ICI, on va reseter les DROPPED -> FREE
      return false
    else
      @cards[i].state = "TAKEN"
      return @cards[i]
    end

  end

  def defausser(carte)
    carte.state = "DROPPED"
  end


end
class Player
  attr_accessor :name, :hand, :distance,  :blocks
  # state = "STOPPED","LIMITED","FREE"

  def initialize(*args)
    @hand = []
    @name = args[0]
    @distance=0
    @blocks= ["LIMITED"]
    for i in 1..6 do
      @hand << $mon_deck.pick_card()
    end
  end

  def play_card(card)
    card.state = "PLAYED"
    card.playable="NON"

    # carte Distance.
    if (card.libelle=="DISTANCE") then
      @distance=@distance+card.libelle.to_i
    end

  end


  def pioche()
    @hand << $mon_deck.pick_card()
  end

  def can_play()
    playable=false
    nbcarte=0
    @hand.each do |card|
      playable = case
        when card.type=="ATTAQUE" then true
        when card.type=="BOTTE" then true
        when card.type=="DEFENSE" then false
        when card.type=="DISTANCE" then false
      end
      playable = true if
        (@blocks.include?("CREVAISON") && card.libelle=="ROUE_SEC") or
        (@blocks.include?("PANNE_ESS") && card.libelle=="ESSENCE") or
        (@blocks.include?("ACCIDENT") && card.libelle=="REPARATION") or
        (@blocks.include?("FEU_ROUGE") && card.libelle=="FEU_VERT") or
        (@blocks.include?("LIMITED") && card.libelle=="25")   or
        (@blocks.include?("LIMITED") && card.libelle=="50") or
        (@blocks.count==0 && une_carte.type=="DISTANCE")
      nbcarte += 1 if playable
      card.playable=playable
    end
    return nbcarte>0 ? true :  false
  end


  def my_turn()
    pioche()
    if can_play()==false then
      ask_defausse()
    else
      ask_play()
    end
  end

  def afficher_main()
    i=1
    @hand.each do |card|
      puts "#{i} - #{card.libelle} : #{card.playable}"
      i=i+1
    end
  end

  def ask_play()
    puts "JOUER :"
    afficher_main()
    input = ask "-> Carte à jouer : "
    carte_jouer= @hand[(input.to_i-1)]
    play_card(carte_jouer)
    @hand.delete_at(input.to_i-1)
  end

  def ask_defausse()
    puts "DEFAUSSER :"
    afficher_main()
    input = ask "-> Carte à defausser : "
    carte_defausser = @hand[(input.to_i-1)]
    $mon_deck.defausser(carte_defausser)
    @hand.delete_at(input.to_i-1)
  end
end
class Carte
  attr_accessor :libelle, :type, :state, :playable
  #state = FREE / PLAYED / DROPPED / TAKEN
  def initialize()
    @state='FREE'
    @playable='YES'
  end
end
$mon_deck = Deck.new()
#  Initialisation
playerA = Player.new("Joueur A")
# On commence à jouer
#while true do
  puts "JOUEUR A (#{playerA.distance})"
  playerA.my_turn()
  playerA.afficher_main

#end
