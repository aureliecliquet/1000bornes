class Deck
  attr_accessor :cards
  def initialize()
    descriptions = [
    ["ACCIDENT",3,"ATTAQUE","millebornes_a-accident.svg.png"],
    ["PANNE_ESS",3,"ATTAQUE","millebornes_a-panne.svg.png"],
    ["CREVAISON",3,"ATTAQUE","millebornes_a-roue.svg.png"],
    ["LIMIT_VIT",4,"ATTAQUE","millebornes_a-50.svg.png"],
    ["FEU_ROUGE",5,"ATTAQUE","millebornes_a-feu.svg.png"],
    ["REPARATION",6,"DEFENSE","millebornes_d-accident.svg.png"],
    ["ESSENCE",6,"DEFENSE","millebornes_d-panne.svg.png"],
    ["ROUE_SEC",6,"DEFENSE","millebornes_d-roue.svg.png"],
    ["LIMIT_FIN",6,"DEFENSE","millebornes_d-50.svg.png"],
    ["FEU_VERT",14,"DEFENSE","millebornes_d-feu.svg.png"],
    ["AS_DU_VOLANT",1,"BOTTE","millebornes_0-accident.svg.png"],
    ["INCREVABLE",1,"BOTTE","millebornes_0-roue.svg.png "],
    ["CITERNE",1,"BOTTE","millebornes_0-panne.svg.png"],
    ["PRIORITAIRE",1,"BOTTE","millebornes_0-50.svg.png"],
    ["25",10,"DISTANCE","millebornes_k-025.svg.png"],
    ["50",10,"DISTANCE","millebornes_k-050.svg.png"],
    ["75",10,"DISTANCE","millebornes_k-075.svg.png"],
    ["100",12,"DISTANCE","millebornes_k-100.svg.png"],
    ["200",4,"DISTANCE","millebornes_k-200.svg.png"]]
    puts "New deck created..."
    @cards=[]

    #Generating deck :
    descriptions.each  do |description|
      for i in 1..description[1] do
        une_carte=Carte.new
        une_carte.libelle=description[0]
        une_carte.type=description[2]
        une_carte.url=description[3]
        add_card(une_carte)
      end
    end
    #calling self method "melange"
    melange
    end

  def add_card(card)
    @cards << card
  end

  def melange()
    puts "Cards shuffled..."
    @cards.shuffle!
  end

  def afficher_jeu()
    for i in 1..(@cards.count) do
      puts "#{@cards[i-1].libelle} -> #{@cards[i-1].state} : #{@cards[i-1].playable}"
    end
  end

  def pick_card()
    # point towards the last card (0)
    i=0
    while i<@cards.count && @cards[i].state != "FREE" do
      i=i+1
    end
    if (i==(@cards.count)) then
      puts "plus de carte !!!"
      # ICI, on va reseter les DROPPED -> FREE
      return false
    else
      @cards[i].state = "TAKEN"
      return @cards[i]
    end

  end

  def defausser(carte)
    carte.state = "DROPPED"
  end


end
class Player
  attr_accessor :name, :hand, :distance,  :blocks
  # state = "STOPPED","LIMITED","FREE"

  def initialize(*args)
    @hand = []
    @name = args[0]
    @distance=0
    @blocks= ["FEU_ROUGE"]
    for i in 1..6 do
      @hand << $mon_deck.pick_card()
    end
  end

  def play_card(card)
    card.state = "PLAYED"
    card.playable="NON"

    # carte Distance.
    if (card.libelle=="DISTANCE") then
      @distance=@distance+card.libelle.to_i
    end

  end


  def pioche()
    @hand << $mon_deck.pick_card()
  end

  def can_play()
    playable=false
    nbcarte=0
    @hand.each do |card|
      playable = case
        when card.type=="ATTAQUE" then true
        when card.type=="BOTTE" then true
        when card.type=="DEFENSE" then false
        when card.type=="DISTANCE" then false
      end
      playable = true if
        (@blocks.include?("CREVAISON") && card.libelle=="ROUE_SEC") or
        (@blocks.include?("PANNE_ESS") && card.libelle=="ESSENCE") or
        (@blocks.include?("ACCIDENT") && card.libelle=="REPARATION") or
        (@blocks.include?("FEU_ROUGE") && card.libelle=="FEU_VERT") or
        (@blocks.include?("LIMITED") && card.libelle=="25")   or
        (@blocks.include?("LIMITED") && card.libelle=="50") or
        (@blocks.count==0 && une_carte.type=="DISTANCE")
      nbcarte += 1 if playable
      card.playable=playable
    end
    puts "canplay #{playable}"
    return nbcarte>0 ? true :  false
  end


  def my_turn(shoes)
    pioche()
    if can_play()==false then
      ask_defausse(shoes)
    else
      ask_play(shoes)
    end
  end

  def afficher_main(shoes)
    i=1
    puts "afficher main..."
    @hand.each do |card|
      puts "#{i} - #{card.libelle} "
      shoes.app do
          @p = para "#{i} - #{card.libelle} "
          if (card.playable)
            @p.style size: 12, stroke: green, margin: 15, top: 100+15*i
          else
            @p.style size: 12, stroke: red, margin: 15, top: 100+15*i
          end
          image  "./pics/#{card.url}", top:  50, left: 10+65*i, width: 60
      end
      i=i+1
    end
  end

  def ask_play(shoes)
    puts "JOUER :"
    afficher_main(shoes)
  # /*input = ask "- Carte a jouer : "
  # carte_jouer= @hand[(input.to_i-1)]
  # play_card(carte_jouer)
  # @hand.delete_at(input.to_i-1)   */
  end

  def ask_defausse(shoes)
    puts "DEFAUSSER :"
    afficher_main(shoes)
  # /*input = ask "-> Carte a defausser : "
  # carte_defausser = @hand[(input.to_i-1)]
  # $mon_deck.defausser(carte_defausser)
  # @hand.delete_at(input.to_i-1)*/
  end
end
class Carte
  attr_accessor :libelle, :type, :state, :playable, :url
  #state = FREE / PLAYED / DROPPED / TAKEN
  def initialize()
    @state='FREE'
    @playable='YES'
  end
end
$mon_deck = Deck.new()
Shoes.app do
 stack do
   para 'hello'
  #  Initialisation
  playerA = Player.new("Joueur A")
  computer = Player.new("Computer")
  # On commence à jouer
  #while true do
  puts "JOUEUR A (#{playerA.distance})"
  playerA.my_turn(self)
  #playerA.afficher_main(self)
  #puts "Computer (#{computer.distance})"
  #computer.my_turn(self)

   end
 end

#end
